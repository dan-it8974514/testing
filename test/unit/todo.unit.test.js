const _ = require('lodash')
const ToDoService = require('../../todo.service')

test('list todos', () => {
    const service = ToDoService()
    const result = service.list()

    expect(_.isArray(result)).toBe(true)
    expect(result.length).toBe(0)
})

test('add todo', () => {
    const service = ToDoService()
    const todo = {
        text: 'test',
        done: false
    }

    const result = service.add(todo)

    expect(_.isNumber(result.id)).toBe(true)
    expect(_.isString(result.task)).toBe(true)
    expect(_.isBoolean(result.done)).toBe(true)

    expect(Object.keys(result)).toEqual(['id', 'task', 'done'])
})

test('get todo', () => {
    const service = ToDoService()
    const todo = {
        text: 'test',
        done: false
    }

    service.add(todo)

    const result = service.get(1)

    expect(_.isNumber(result.id)).toBe(true)
    expect(_.isString(result.task)).toBe(true)
    expect(_.isBoolean(result.done)).toBe(true)

    expect(Object.keys(result)).toEqual(['id', 'task', 'done'])

    expect(result.task).toBe('test')
    expect(result.done).toBe(false)
})

test('remove todo', () => {
    const service = ToDoService()
    const todo = {
        text: 'test',
        done: false
    }

    service.add(todo)

    const removeResult = service.remove(1)
    const listResult = service.list()

    expect(_.isNumber(removeResult.id)).toBe(true)
    expect(_.isString(removeResult.task)).toBe(true)
    expect(_.isBoolean(removeResult.done)).toBe(true)

    expect(removeResult).toHaveProperty('id', 1)

    expect(listResult.find((t) => t.id === removeResult.id)).toBeUndefined()
})

test('update todo', () => {
    const service = ToDoService()
    const todo = {
        text: 'test',
        done: false
    }

    service.add(todo)

    const updateResult = service.update(1, {
        text: 'test2',
        done: true
    })

    expect(_.isNumber(updateResult.id)).toBe(true)
    expect(_.isString(updateResult.task)).toBe(true)
    expect(_.isBoolean(updateResult.done)).toBe(true)

    expect(Object.keys(updateResult)).toEqual(['id', 'task', 'done'])

    const result = service.get(1)

    expect(result.task).toBe('test2')
})
