const UserService = require('../../user.service')
const ToDoService = require('../../todo.service')

describe('user with todo actions', () => {
    const userService = UserService()
    const todoService = ToDoService()

    test('create user with actions and call action add todo', () => {
        const user = {
            name: 'test'
        }

        userService.create(user)

        userService.addActions({
            addTodo: (todo) => {
                todoService.add(todo)
            }
        })

        userService.callAction('addTodo', {
            text: 'test',
            done: false
        })

        const list = todoService.list()

        expect(list.length).toBe(1)

        const todo = list[0]

        expect(todo.task).toBe('test')
    })
})