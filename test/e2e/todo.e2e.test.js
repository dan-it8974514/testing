const _ = require('lodash')
const ToDoService = require('../../todo.service')

describe('e2e todo service', () => {
    const service = ToDoService()

    beforeEach(() => {
        
    })

    afterEach(() => {
        service.list().forEach((todo) => {
            service.remove(todo.id)
        })
    })

    test('is todos list empty', () => {
        const result = service.list()
        expect(_.isArray(result)).toBe(true)
        expect(result.length).toBe(0)
    })

    test('add todo to list', () => {
        const todo = {
            text: 'test',
            done: false
        }
        const result = service.add(todo)
        
        const list = service.list()

        expect(result.id).toBe(list[0].id)
        expect(result.task).toBe(list[0].task)
        expect(result.done).toBe(list[0].done)

        expect(list).toHaveLength(1)
    })

    test('add second todo to list', () => {
        const todo = {
            text: 'test2',
            done: false
        }
        const result = service.add(todo)
        
        const list = service.list()

        expect(result.id).toBe(list[1].id)
        expect(result.task).toBe(list[1].task)
        expect(result.done).toBe(list[1].done)

        expect(list.length).toBe(2)
    })

    test('get todo from list', () => {
        const result = service.get(1)

        expect(result.id).toBe(1)
        expect(result.task).toBe('test')
        expect(result.done).toBe(false)

        const list = service.list()

        expect(list.length).toBe(2)
    })

    test('update todo in list', () => {
        const todo = {
            text: 'update',
            done: true
        }
        const result = service.update(1, todo)

        const item = service.get(1)

        expect(item.id).toBe(1)
        expect(item.task).toBe('update')
        expect(item.done).toBe(true)

        expect(result.id).toBe(item.id)
        expect(result.task).toBe(item.task)
        expect(result.done).toBe(item.done)
    })


    test('remove todo from list', () => {
        service.remove(1)

        const list = service.list()

        expect(list[0].id).toBe(2)
        expect(list.length).toBe(1)
    })
})