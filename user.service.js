const UserService = () => {
    let user = null

    const create = (newUser) => {
        user = newUser
    }

    const get = () => {
        return user
    }

    const addActions = (actions) => {
        user.actions = {
            ...user.actions,
            ...actions
        }
    }

    const callAction = (action, data) => {
        user.actions[action](data)
    }

    return {
        user,
        create,
        get,
        addActions,
        callAction
    }
}

module.exports = UserService