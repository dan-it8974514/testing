const ToDoService = () => {
    const todos = []

    const add = (todo) => {
        const item = {
            id: todos.length + 1,
            task: todo.text,
            done: todo.done
        }

        todos.push(item)
        return item
    }

    const list = () => {
        return todos
    }

    const get = (id) => {
        return todos.find((todo) => todo.id === id)
    }

    const remove = (id) => {
        const todo = todos.find((todo) => todo.id === id)
        todos.splice(todos.indexOf(todo), 1)

        return todo
    }

    const update = (id, todo) => {
        const todoIndex = todos.findIndex(item => item.id === id)
        if (todoIndex !== -1) {
            todos[todoIndex] = {
                ...todos[todoIndex],
                task: todo.text || todos[todoIndex].task,
                done: todo.done || todos[todoIndex].done
            }
            return todos[todoIndex]
        }

        return null
    }

    return { add, get, list, remove, update }
}

module.exports = ToDoService